package net.Indyuce.inventory.manager.data.sql;

import io.lumine.mythic.lib.UtilityMethods;
import io.lumine.mythic.lib.data.DefaultOfflineDataHolder;
import io.lumine.mythic.lib.data.sql.SQLDataSource;
import io.lumine.mythic.lib.data.sql.SQLDataSynchronizer;
import io.lumine.mythic.lib.data.sql.SQLSynchronizedDataHandler;
import net.Indyuce.inventory.MMOInventory;
import net.Indyuce.inventory.inventory.CustomInventoryHandler;
import net.Indyuce.inventory.inventory.InventoryHandler;
import net.Indyuce.inventory.inventory.InventoryItem;
import net.Indyuce.inventory.inventory.InventoryLookupMode;
import net.Indyuce.inventory.manager.data.yaml.YAMLDataHandler;
import org.apache.commons.lang3.Validate;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;

public class SQLDataHandler extends SQLSynchronizedDataHandler<InventoryHandler, DefaultOfflineDataHolder> {
    public SQLDataHandler(SQLDataSource dataSource) {
        super(dataSource);
    }

    private static final String[] NEW_COLUMNS = new String[]{
            "is_saved", "TINYINT"};

    @Override
    public void setup() {
        getDataSource().executeUpdate("CREATE TABLE IF NOT EXISTS mmoinventory_inventories (" +
                "uuid VARCHAR(36) NOT NULL," +
                "inventory LONGTEXT," +
                "is_saved TINYINT," +
                "PRIMARY KEY (uuid));");

        // Nullable inventory
        getDataSource().executeUpdate("ALTER TABLE `mmoinventory_inventories` MODIFY `inventory` LONGTEXT;");

        // Add columns that might not be here by default
        for (int i = 0; i < NEW_COLUMNS.length; i += 2) {
            final String columnName = NEW_COLUMNS[i];
            final String dataType = NEW_COLUMNS[i + 1];
            getDataSource().getResultAsync("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = 'mmoinventory_inventories' AND COLUMN_NAME = '" + columnName + "'", result -> {
                try {
                    if (!result.next())
                        getDataSource().executeUpdate("ALTER TABLE mmoinventory_inventories ADD COLUMN " + columnName + " " + dataType);
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            });
        }
    }

    @Override
    public boolean loadData(@NotNull InventoryHandler playerData) {
        return playerData instanceof CustomInventoryHandler && this.newDataSynchronizer(playerData).synchronize();
    }

    @Override
    public SQLDataSynchronizer newDataSynchronizer(InventoryHandler playerData) {
        Validate.isInstanceOf(CustomInventoryHandler.class, playerData, "Cannot load data from SQL when vanilla inventory is in use");
        return new MMOInventoryDataSynchronizer(getDataSource(), (CustomInventoryHandler) playerData);
    }

    @Override
    public void saveData(InventoryHandler data, boolean autosave) {

        if (!(data instanceof CustomInventoryHandler))
            return;

        final UUID effectiveId = data.getEffectiveId();
        try {

            final Collection<InventoryItem> items = data.getItems(InventoryLookupMode.IGNORE_RESTRICTIONS);
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            final BukkitObjectOutputStream bukkitAdapter = new BukkitObjectOutputStream(outputStream);

            // Serialize items
            bukkitAdapter.writeInt(items.size());
            for (InventoryItem entry : items) {
                final int slot = entry.getSlot().getIndex();
                bukkitAdapter.writeInt(slot);
                bukkitAdapter.writeObject(entry.getItemStack());
            }

            // Encode serialized object into to the Base64 format and execute update
            bukkitAdapter.close();
            final String base64 = new String(Base64.getEncoder().encode(outputStream.toByteArray()));

            try {
                final Connection connection = getDataSource().getConnection();
                final PreparedStatement prepared = connection.prepareStatement("INSERT INTO mmoinventory_inventories (uuid, inventory, `is_saved`) VALUES(?,?,?) ON DUPLICATE KEY UPDATE inventory = VALUES(`inventory`), `is_saved` = VALUES(`is_saved`);");

                try {
                    UtilityMethods.debug(MMOInventory.plugin, "SQL", "Saving data of '" + effectiveId + "'");
                    prepared.setString(1, effectiveId.toString());
                    prepared.setString(2, base64);
                    prepared.setInt(3, autosave ? 0 : 1);
                    prepared.executeUpdate();
                    UtilityMethods.debug(MMOInventory.plugin, "SQL", "Saved data of '" + effectiveId + "'");
                } catch (Throwable throwable) {
                    MMOInventory.plugin.getLogger().log(Level.WARNING, "Could not save player inventory of " + data.getPlayer().getName() + " (" + effectiveId + ")");
                    throwable.printStackTrace();
                } finally {

                    // Close statement and connection to prevent leaks
                    prepared.close();
                    connection.close();
                }
            } catch (SQLException exception) {
                MMOInventory.plugin.getLogger().log(Level.WARNING, "Could not save player inventory of " + data.getPlayer().getName() + " (" + effectiveId + "), saving in YAML instead");
                exception.printStackTrace();

                // Save in YAML
                new YAMLDataHandler().saveData(data, autosave);
            }

        } catch (IOException exception) {
            MMOInventory.plugin.getLogger().log(Level.WARNING, "Could not save player inventory of " + data.getPlayer().getName() + " (" + effectiveId + ")");
            exception.printStackTrace();
        }
    }

    @Override
    public DefaultOfflineDataHolder getOffline(UUID uuid) {
        return new DefaultOfflineDataHolder(uuid);
    }
}
