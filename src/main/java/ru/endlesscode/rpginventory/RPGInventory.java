package ru.endlesscode.rpginventory;

import io.lumine.mythic.lib.util.MMOPlugin;
import net.Indyuce.inventory.MMOInventory;

/**
 * @deprecated illegal workaround
 */
@Deprecated
public class RPGInventory extends MMOPlugin {
    public static RPGInventory getInstance() {
        return MMOInventory.plugin;
    }
}
